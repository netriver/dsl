
FROM php:7.3.1-cli

EXPOSE 9095

RUN mkdir -p /usr/src/router_zyxel/
COPY * /usr/src/router_zyxel/

WORKDIR /usr/src/router_zyxel/

CMD [ "php", "-S", "0.0.0.0:9095" ]
